# TreeTest-method-in-Java



public class TreeTest {
 
 public static void main(String[] args) {
    SearchTree t = new SearchTree();
    
    // Insert 12 names and nicknames in the tree
    t.insert("Richard");
    t.insert("Debbie");
    t.insert("Robin");
    t.insert("Chris");
    t.insert("Jackquie");
    t.insert("Jeffrey");
    t.insert("Jackqueline");
    t.insert("Christopher");
    
    // Display the tree contents
    System.out.println(t.toString());
   }
  }  
  
